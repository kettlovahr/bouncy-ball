#include <raylib.h>

typedef struct {
    float x;
    float y;
    float xspeed;
    float yspeed;
    float r;
    float bounciness;
    Color color;
    int streak;
} Ball;
