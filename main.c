#include <raylib.h>
#include "game.c"

int main(void) {
    InitWindow(WIDTH, HEIGHT, "Kett's Raylib Test");

    GameState game = create_gamestate();
    Ball* ball = create_ball(&game, 100, 100, 120, MAGENTA);
    Ball* dawn = create_ball(&game, 300, 300, 80, BLUE);
    Ball* ball2 = create_ball(&game, 500, 200, 45, YELLOW);
    Ball* ball3 = create_ball(&game, 700, 100, 60, WHITE);
    Ball* ball4 = create_ball(&game, 200, 300, 90, RED);
    dawn->bounciness = 0.1;

    SetTargetFPS(60);

    while (!WindowShouldClose()) {

        if (IsKeyPressed(KEY_Q)) {
            TakeScreenshot("screenshot.png");
        }

        if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
            get_ball_click(&game, GetMousePosition());
        }

        game_loop(&game);

        BeginDrawing();
        ClearBackground(BLACK);
        draw_loop(&game);

        update_score(&game);
        EndDrawing();
    }
    CloseWindow();
    return 0;
}
